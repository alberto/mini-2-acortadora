from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt  # Saltar mecanismo de seguridad
from .models import Links
from django.template import loader
from django.shortcuts import redirect
import string
import random

# Create your views here.
@csrf_exempt
def index(request):
    if request.method=="POST":
        Valor = request.POST["url"]
        Short = request.POST["short"]
        Url = http_check(Valor)
        try:  # POST
            if Valor!="":
                Links.objects.get(url=Valor)# si la url ya existe no se guardara ni se eliminara el anterior
                if Short!="":
                    Links.object.get(acorte=Short)# si el acorte ya existe no guardamos ni añadimos nada
        except Links.DoesNotExist:
            if Short == "":
                contador = 1
                try:
                    while True:
                        Links.objects.get(acorte=contador)
                        contador = contador + 1
                except Links.DoesNotExist:
                    data = Links(url=Url, acorte=contador)
            else:
                data = Links(url=Url, acorte=Short)
            data.save()
    link_list=Links.objects.all()
    template=loader.get_template('acorte/index.html')
    contexto={'link_list':link_list}
    return HttpResponse(template.render(contexto,request))

def http_check(url):
    if not (url.startswith("http://")) and not url.startswith("https://"):
        url = "https://" + url
    return url

@csrf_exempt
def get_resource(request, recurso):
    if request.method=="POST":
        Valor = request.POST["url"]
        Short =request.POST["short"]
        Url=http_check(Valor)
        try:#POST
            if Valor != "":
                Links.objects.get(url=Valor)  # si la url ya existe no se guardara ni se eliminara el anterior
                if Short != "":
                    Links.object.get(acorte=Short)  # si el acorte ya existe no guardamos ni añadimos nada
        except Links.DoesNotExist:
            if Short=="":
                contador=1
                try:
                    while True:
                        Links.objects.get(acorte=contador)
                        contador=contador+1
                except Links.DoesNotExist:
                    data = Links(url=Url, acorte=contador)
            else:
                data = Links(url=Url, acorte=Short)
            data.save()
    elif request.method=="GET":
        try:#GET
            link = Links.objects.get(acorte=recurso)
            #Aqui haces la redireccion
            return redirect(link.url)
        except Links.DoesNotExist:
            link_list = Links.objects.all()
            template = loader.get_template('acorte/index.html')
            contexto = {'link_list': link_list}
            return HttpResponse(template.render(contexto, request))
    link_list = Links.objects.all()
    template = loader.get_template('acorte/index.html')
    contexto = {'link_list': link_list}
    return HttpResponse(template.render(contexto, request))