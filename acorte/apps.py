from django.apps import AppConfig


class AcorteConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "acorte"
